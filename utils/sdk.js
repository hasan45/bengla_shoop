const sharetribeSdk = require('sharetribe-flex-sdk');

const sdkProvider = () => {
    // ShareTribe info
    const clientId = process.env.FLEX_MARKETPLACE_API_CLIENT_ID;
    const sdk = sharetribeSdk.createInstance({
        clientId: clientId,
        baseUrl: 'https://flex-api.sharetribe.com/',
    });
    return sdk
}

export default sdkProvider