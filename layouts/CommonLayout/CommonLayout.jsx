import Footer from "../Footer";
import Navbar from "../Navbar";

const CommonLayout = ({ children }) => {
    return <div>
        <Navbar />
        <main className="px-3 sm:px-5 md:px-8 xl:px-12">
            {children}
        </main>
        <Footer />
    </div>;
};
export default CommonLayout;