import { FaFacebookF, FaInstagram, FaSkype, FaTwitter } from 'react-icons/fa';
import IconContainer from './components/IconContainer';


const BootomPart = () => {
    return <div className="grid grid-col-1 md:grid-cols-2 xl:grid-cols-3 gap-5 items-center mt-20">

        <div className="grid grid-cols-4 w-64 xl:order-1 mx-auto xl:mx-0">
            <IconContainer>
                <FaFacebookF />
            </IconContainer>

            <IconContainer>
                <FaInstagram />
            </IconContainer>

            <IconContainer>
                <FaTwitter />
            </IconContainer>

            <IconContainer>
                <FaSkype />
            </IconContainer>
        </div>

        <div className="xl:order-3 mx-auto xl:ml-auto">
            <img className="h-10 mx-auto md:ml-auto" src="/images/logo/paymentLogo.png" alt="payment logo" />
        </div>

        <div className="text-center md:col-span-2 xl:col-span-1 xl:order-2 text-[#828282]">
            <small>@2022 Copyright All Right Reserved by Bengal Shop</small>
        </div>


    </div>;
};
export default BootomPart;