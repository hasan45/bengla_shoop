const IconContainer = ({ children }) => {
    return <div className="hover:bg-[#FF5E4D] w-14 h-14 flex justify-center items-center bg-white rounded-md text-text_color hover:text-white">
        {children}
    </div>;
};
export default IconContainer;