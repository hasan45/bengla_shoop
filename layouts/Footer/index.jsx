import Logo from '../../components/reuseable/Logo';
import AboutUs from './components/AboutUs/index'
import BootomPart from './components/BottomPart';
import Info from './components/Info/index'

const Footer = () => {
    return <div className="w-full bg-[#F5F5F5] p-5 sm:p-20 lg:p-24 mt-24">
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-y-14 lg:gap-y-0 gap-x-0 lg:gap-x-10">

            <div className='order-2 lg:order-1'>
                <Logo />
                <p className="text-text_color max-w-lg text-justify mt-12">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla diam ornare nam est gravida. Netus viverra rhoncus sit magna sapien ac eget parturient id. Est luctus dapibus quam aliquam in nisl turpis. Elit et dictum lacus morbi nec accumsan a in.
                </p>
                <div className='h-10 sm:h-12 flex mt-12'>
                    <img className='h-full mr-2' src={'/images/logo/appleStore.png'} alt="apple store" />
                    <img className='h-full' src={'/images/logo/playStore.png'} alt="google store" />
                </div>
            </div>

            <div className='grid grid-cols-2 gap-3 order-1 lg:order-2'>
                <AboutUs />
                <Info />
            </div>
        </div>

        <BootomPart />

    </div>;
};
export default Footer;