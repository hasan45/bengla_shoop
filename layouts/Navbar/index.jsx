/* eslint-disable @next/next/no-img-element */
import { BiUser } from "react-icons/bi";
import { FaSearch } from "react-icons/fa";
import { HiMenuAlt2 } from "react-icons/hi";
import { BsChevronDown } from "react-icons/bs";
import { AiFillCaretDown, AiOutlineHeart, AiOutlineShopping } from "react-icons/ai";
import Link from "next/link";
import Logo from "../../components/reuseable/Logo";
import { useState } from "react";
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from "react-redux";
import { loadUserData } from "../../store/currentUserSlice";
import sdkProvider from "../../utils/sdk";
import isUser from "../../utils/isUser";

export default function Navbar() {

    // ShareTribe info
    const sdk = sdkProvider()

    const dispatch = useDispatch()
    // const currentUserId = useSelector(state => state?.currentUser?.user);

    const { authUser, loading } = isUser();

    const [openMenu, setOpenMenu] = useState()
    const router = useRouter()

    const cart = useSelector(state => state.cart.cart)
    const cartLength = cart.length

    return <div className="mx-5 lg:mx-16 py-6">

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 items-center">
            <div className="flex justify-center md:justify-start">
                <Logo />
            </div>

            <div className="mx-auto border-[1px] p-1 border-[#E0E0E0] h-10 lg:h-14 rounded-full justify-between hidden lg:flex lg:w-[400px] mt-4 lg:mt-0">
                <div className="flex items-center px-5">
                    <FaSearch />
                    <input className="py-2 px-3" type="text" placeholder='Search here' />
                </div>
                <button className="bg-[#4F4F4F]  px-5 rounded-full text-white cursor-pointer">Search</button>
            </div>

            <div className="md:flex md:justify-end mt-3 ml-3 xl:mt-0 xl:ml-0 space-x-5 items-center hidden ">
                <p className="cursor-pointer"><AiOutlineHeart className="text-lg" /></p>

                <p onClick={() => router.replace('/cart')}
                    className="cursor-pointer relative">
                    <AiOutlineShopping className="text-lg" />
                    <span className="absolute top-[-20px] right-[-15px] h-5 w-5 flex justify-center items-center bg-orange1 rounded-full text-white text-xs">{cartLength}</span>
                </p>

                <div className="flex items-center">
                    {/* <p className="bg-[#F2F2F2] flex justify-center items-center h-12 w-12 mr-1 rounded-full"><BiUser /></p> */}
                    <Link href={'/profile'}>
                        <a className="bg-[#F2F2F2] flex justify-center items-center h-12 w-12 mr-1 rounded-full">
                            <BiUser /></a>
                    </Link>

                    {
                        // currentUserId?.email ?
                        authUser ?
                            <button onClick={() => {
                                sdk.logout().then(loginRes => {
                                    console.log("Logout successful.")
                                    console.log(loginRes)
                                    router.push('/')
                                    toast("Logout Successfull")
                                    dispatch(loadUserData({}))

                                });
                            }}
                                className="text-red-500 font-semibold ml-1">
                                Logout
                            </button>
                            :
                            <Link href={'/login'} className="cursor-pointer">
                                <a className="text-myGreen font-semibold ml-1">
                                    Login
                                </a>
                            </Link>
                    }

                </div>
            </div>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 mt-6">

            <div
                className="lg:w-3/4 flex justify-evenly items-center text-white bg-green  cursor-pointer rounded-full py-3 mb-3">
                <p className="px-2"><HiMenuAlt2 /></p>
                <button className="px-2">All Categories</button>
                <p className="px-2"><BsChevronDown /></p>
            </div>

            <div className="hidden lg:flex justify-end xl:justify-center items-center">
                <ul className="flex xl:space-x-8 xs:space-x-6 space-x-4 text-sm">
                    <li className="cursor-pointer flex justify-between items-center space-x-2">
                        <Link href='/'>
                            <a>Home</a>
                        </Link>
                        <AiFillCaretDown />
                    </li>
                    <li className="cursor-pointer flex justify-between items-center space-x-2">
                        <Link href='/shop'>
                            <a>Shop</a>
                        </Link>
                        <AiFillCaretDown />
                    </li>
                    <li className="cursor-pointer flex justify-between items-center space-x-2">
                        <p>Pages</p>
                        <AiFillCaretDown />
                    </li>

                    <Link href={'/formik'}>
                        <a className="smd:flex items-center hidden flex justify-between items-center space-x-2">
                            Formik
                            <AiFillCaretDown />
                        </a>
                    </Link>

                    <Link href={'/addListing'}>
                        <a className="smd:flex items-center hidden">
                            Add Listing
                        </a>
                    </Link>
                </ul>
            </div>
            <div className="hidden xl:flex xl:justify-end items-center mt-5 lg:ml-3 xl:mt-0 xl:ml-0">
                <p className="text-orange1">% Special Offers!</p>
            </div>


            {/* Open the menu button container Small device */}
            <div className="relative z-50 bg-white lg:hidden">
                <div onClick={() => setOpenMenu(!openMenu)} className='lg:hidden text-right'>
                    <img src="https://img.icons8.com/fluency/48/undefined/menu--v1.png" alt='nav menu' className='w-7 h-7 ml-auto mr-2' />
                </div>
                <div className={`${openMenu ? 'top-6' : 'top-[-600px] h-6 bg-red-400 duration-300 ease-linear'} absolute w-full bg-white`}>

                    <div className="flex md:hidden justify-center mb-5 space-x-5 items-center">
                        <p className="cursor-pointer"><AiOutlineHeart /></p>
                        <p className="cursor-pointer"><AiOutlineShopping /></p>

                        <div className="flex items-center">
                            <p className="bg-[#F2F2F2] flex justify-center items-center h-12 w-12 mr-1 rounded-full"><BiUser /></p>
                            <p className="text-custom_gray2 cursor-pointer">Account</p>
                        </div>
                    </div>

                    <div className="flex justify-center items-center my-2 text-center bg-white">
                        <Link href='/'>
                            <a>Home</a>
                        </Link>
                        <AiFillCaretDown />
                    </div>
                    <div className="flex justify-center items-center my-2 text-center bg-white">
                        <Link href='/shop'>
                            <a>Shop</a>
                        </Link>
                        <AiFillCaretDown />
                    </div>
                    <div className="flex justify-center items-center my-2 text-center bg-white">
                        <p>Pages</p>
                        <AiFillCaretDown />
                    </div>

                    <div className="flex justify-center items-center my-2 text-center bg-white">
                        <p>Blogs</p>
                        <AiFillCaretDown />
                    </div>

                    <div className="flex justify-center items-center my-2 text-center bg-white">
                        <p>Contact</p>
                    </div>

                    <div className="pb-3 text-center">
                        <p className="text-orange1">% Special Offers!</p>
                    </div>

                </div>
            </div>


        </div>
    </div >
}