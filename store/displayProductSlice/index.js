import { createSlice } from '@reduxjs/toolkit'

const initialState = { displayProduct: [] }

export const displayProductSlice = createSlice({
    name: 'displayProduct',
    initialState,
    reducers: {
        loadDisplayProduct: (state, action) => {
            state.displayProduct = action.payload
        },
    },
})

export const { loadDisplayProduct } = displayProductSlice.actions
export default displayProductSlice.reducer