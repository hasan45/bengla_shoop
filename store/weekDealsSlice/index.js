import { createSlice } from '@reduxjs/toolkit'

const initialState = { weekDeals: [] }

export const weekDealsSlice = createSlice({
    name: 'weekDeals',
    initialState,
    reducers: {
        loadWeekDeals: (state, action) => {
            state.weekDeals = action.payload
        },
    },
})

export const { loadWeekDeals } = weekDealsSlice.actions
export default weekDealsSlice.reducer