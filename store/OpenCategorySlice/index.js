import { createSlice } from '@reduxjs/toolkit'

const initialState = { openCategory: false }

export const openCategory = createSlice({
    name: 'categoryMenu',
    initialState,
    reducers: {
        change: (state) => {
            state.openCategory = !state.openCategory
        },
    },
})

export const { change } = openCategory.actions
export default openCategory.reducer