import { createSlice } from '@reduxjs/toolkit'

const initialState = { cart: [] }

export const cartSlice = createSlice({
    name: 'Cart',
    initialState,
    reducers: {
        addToCart: (state, action) => {
            const cartData = {
                product: action.payload.product,
                quantity: action.payload.productAmount
            }
            state.cart = [...state.cart, cartData]
        },
    },
})

export const { addToCart } = cartSlice.actions
export default cartSlice.reducer