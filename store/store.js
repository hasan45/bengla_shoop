import { configureStore } from '@reduxjs/toolkit'
import category from './categorySlice/index'
import products from './productsSlice/index'
import weekDeals from './weekDealsSlice/index'
import OpenCategory from './OpenCategorySlice/index'
import displayProduct from './displayProductSlice/index'
import cart from './cartSlice/index'
import currentUser from './currentUserSlice/index'

export const store = configureStore({
    reducer: {
        category,
        products,
        weekDeals,
        OpenCategory,
        displayProduct,
        cart,
        currentUser
    },
})

