import { createSlice } from '@reduxjs/toolkit'

const initialState = { user: {} }

export const currentUserSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        loadUserData: (state, action) => {
            state.user = action.payload
        },
    },
})

export const { loadUserData } = currentUserSlice.actions
export default currentUserSlice.reducer