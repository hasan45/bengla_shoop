/* eslint-disable @next/next/no-img-element */
import { useState } from "react";
import { FaStar } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { addToCart } from "../../../store/cartSlice";
import React from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const DetailsModal = ({ setIsOpen, product }) => {

    const { img, ratting, noOfRatting, name, price, oldPrice, sku, category, subCategory } = product
    const [productAmount, setProductAmount] = useState(1);
    const dispatch = useDispatch()

    return <div className="flex justify-center items-center h-screen w-screen z-[1000]">

        <div className="bg-white relative rounded-3xl p-5">

            <button onClick={() => setIsOpen(false)} className="bg-red-600 text-white px-3 py-1 
            rounded-full font-semibold absolute top-5 right-5">X</button>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-5">
                <div>
                    <img className="w-28 md:w-full" src={img} alt="product" />
                </div>
                <div>
                    <div className="flex">
                        <h1>Status</h1>
                        <h1 className="text-myGreen font-semibold ml-5">In Stock</h1>
                    </div>
                    <div className="text-left">
                        <h1 className="mt-5 text-2xl font-semibold text-heading_color">{name}</h1>

                        <div className="flex text-sm justify-left items-center my-5 text-[#FABE50]">
                            <FaStar />
                            <FaStar />
                            <FaStar />
                            <FaStar />
                            <FaStar />
                            <span className="ml-2 text-text_color">({noOfRatting})</span>
                        </div>
                        <div className="text-3xl flex items-center">
                            <span>${price}</span><span className="text-[#BDBDBD] line-through text-2xl ml-2">${oldPrice}</span><span className="ml-2 text-sm">(15% Vat Included)</span>
                        </div>
                        <p className="my-3">20 products sold in last 12 </p>
                        <hr />

                        <div className="my-5 flex justify-center items-center">
                            <p>Quantity</p>
                            <div className="flex mx-2">
                                <div className="w-10 h-10 mx-1 flex justify-center items-center rounded-full border-2 cursor-pointer" onClick={() => {
                                    if (productAmount > 1) {
                                        setProductAmount(productAmount - 1)
                                    }
                                }}>-</div>


                                <div className="w-10 h-10 mx-1 flex justify-center items-center rounded-full border-2 bg-gray-100 text-myGreen text-2xl">{productAmount}</div>

                                <div className="w-10 h-10 mx-1 flex justify-center items-center rounded-full border-2 cursor-pointer" onClick={() => setProductAmount(productAmount + 1)}>+</div>
                            </div>
                            <p>Only 10 left</p>
                        </div>

                        <div className="grid gap-2 grid-cols-1">
                            <button
                                onClick={() => {
                                    dispatch(addToCart({ product, productAmount }))
                                    toast.success('Added to cart successfully', { id: 'add-cart-modal' })
                                    setIsOpen(false)
                                }}

                                className="w-full py-2 bg-green text-white rounded-full">Add To Cart</button>

                            <button className="w-full py-2 bg-slate-100 rounded-full">Buy Now</button>

                            <p>SKU: {sku}</p>
                            <p>CATEGORY: {category}</p>
                            <p>TAGS: {category}, {subCategory}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>;
};
export default DetailsModal;