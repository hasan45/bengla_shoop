/* eslint-disable @next/next/no-img-element */
import { useState } from "react";
import { FaArrowRight, FaStar } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { addToCart } from "../../../store/cartSlice";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import DetailsModal from "../DetailsModal";

const ProductContainer = ({ product }) => {
    const { img, ratting, noOfRatting, name, price, oldPrice } = product

    const [details, setDetails] = useState({});

    const [modalIsOpen, setIsOpen] = useState(false);

    const dispatch = useDispatch()

    const [productAmount, setProductAmount] = useState(1);

    return <div className="max-w-[318px] relative mx-auto">

        <div className="group relative">
            <img className="rounded-2xl" src={img} alt={name} />

            <div className="text-white transition absolute hidden group-hover:flex h-full w-full bg-[#00000066] top-0 rounded-2xl justify-center items-center">
                <div className="flex">
                    <div className="w-10 h-10 mx-1 flex justify-center items-center rounded-full border-2 border-white  cursor-pointer" onClick={() => {
                        if (productAmount > 1) {
                            setProductAmount(productAmount - 1)
                        }
                    }}>-</div>

                    <div className="w-10 h-10 mx-1 flex justify-center items-center rounded-full border-2 bg-white text-myGreen text-2xl">{productAmount}</div>

                    <div className="w-10 h-10 mx-1 flex justify-center items-center rounded-full border-2 border-white cursor-pointer" onClick={() => setProductAmount(productAmount + 1)}>+</div>
                </div>


                <button onClick={() => setIsOpen(true)} className="absolute bottom-0 bg-[#F2F2F2] text-[#999999] rounded-b-2xl w-full text-center h-11 flex justify-center items-center">
                    <p className="mr-1">Details</p>
                    <FaArrowRight />
                </button>

            </div>

        </div>
        <div className="mt-5 text-center">
            <div className="flex text-sm justify-center items-center mb-5 text-[#FABE50]">
                <FaStar />
                <FaStar />
                <FaStar />
                <FaStar />
                <FaStar />
                <span className="ml-2 text-text_color">({noOfRatting})</span>
            </div>
            <h5 className="text-xl font-medium">{name}</h5>
            <div className="mt-4 text-xl">
                <span>${price}</span><span className="text-[#BDBDBD] line-through text-lg ml-2">${oldPrice}</span>
            </div>

            <button className=" text-myGreen text-sm 3xl:text-lg font-bold hover:bg-green hover:text-white border-2 px-5 md:px-10 py-2 mt-5 rounded-full"
                onClick={() => {
                    dispatch(addToCart({ product, productAmount }))
                    toast.success('Added to cart successfully', { id: 'add-success-now' })
                }}
            >Add To Cart</button>

            <div className={`${modalIsOpen ? 'block' : 'hidden'} fixed top-0 bottom-0 left-0 right-0 z-[1000] bg-[#00000080]`}>
                <DetailsModal setIsOpen={setIsOpen} product={product} />
            </div>

        </div>
        <div className="w-14 h-8 flex justify-center items-center bg-heading_color rounded-full absolute top-5 right-5">
            <p className="text-white">-30%</p>
        </div>

    </div >;
};
export default ProductContainer;