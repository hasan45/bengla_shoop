/* eslint-disable @next/next/no-img-element */
const Logo = () => {
    return <div>
        <div className="flex">
            <img className="h-10 mr-3" src={'/images/logo/logo.png'} alt="Logo" />
            <h3 className="text-3xl font-semibold">Bengal Shop</h3>
        </div>
    </div>;
};
export default Logo;