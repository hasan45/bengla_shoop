/* eslint-disable @next/next/no-img-element */
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// import required modules
import { Pagination, Navigation } from "swiper";
import CategoryContainer from "../../subPages/Home/components/CategoryContainer";
import ProductContainer from "../ProductContainer";

export default function App({ type, dataArray }) {

    return (
        <div className="relative mt-24">
            {
                type === 'category' && <h2 className="text-3xl font-semibold">Search by Category</h2>
            }
            {
                type === 'weekDeals' && <h2 className="text-3xl font-semibold">Deals of the Week</h2>
            }
            {
                type === 'brands' && <h2 className="text-3xl font-semibold">Popular Brands</h2>
            }

            <div className="mt-6">
                <Swiper
                    slidesPerView={1}

                    breakpoints={{
                        "@0.00": {
                            slidesPerView: 1,
                            spaceBetween: 10,
                        },
                        "@0.70": {
                            slidesPerView: 2,
                            spaceBetween: 10,
                        },
                        "@1.00": {
                            slidesPerView: 3,
                            spaceBetween: 40,
                        },
                        "@1.50": {
                            slidesPerView: 4,
                            spaceBetween: 50,
                        },
                        "@2.00": {
                            slidesPerView: 5,
                            spaceBetween: 50,
                        },
                        "@2.60": {
                            slidesPerView: 6,
                            spaceBetween: 50,
                        },
                    }}

                    className="h-full"

                    pagination={{
                        type: "progressbar",
                    }}
                    navigation={true}
                    modules={[Pagination, Navigation]}
                >

                    {/* Category Slider */}
                    {
                        type === 'category' &&
                        dataArray.map(data => <SwiperSlide className="pt-10 flex justify-center items-center" key={data.id}>
                            <CategoryContainer data={data} />
                        </SwiperSlide>)
                    }
                    {/* Deals Of the week slider */}
                    {
                        type === 'weekDeals' &&
                        dataArray.map(product => <SwiperSlide className="pt-10 flex justify-center items-center" key={product.sku}>
                            <ProductContainer product={product} />
                        </SwiperSlide>)
                    }
                    {/* Popular Brands */}
                    {
                        type === 'brands' &&
                        dataArray.map((brand, i) => <SwiperSlide className="pt-10 flex justify-center items-center" key={i}>
                            <img src={brand} alt="brand" />
                        </SwiperSlide>)
                    }
                </Swiper>
            </div>
        </div>
    );
}
