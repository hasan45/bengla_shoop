/* eslint-disable @next/next/no-img-element */
const SpecialityContainer = ({ title, img }) => {
    return <div className="flex">
        <div>
            <img src={img} alt="Speciality Icons" className="w-20" />
        </div>
        <div className="flex items-center ml-3">
            <div>
                <h3 className="text-heading_color text-xl 3xl:text-2xl font-semibold mb-3">{title}</h3>
                <p className="text-[#828282]">{title}</p>
            </div>
        </div>
    </div>;
};
export default SpecialityContainer;