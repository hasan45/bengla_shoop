import SpecialityContainer from "./components/Container";

const Speciality = () => {
    return <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5 mt-24">
        <SpecialityContainer title={'24 Customer Support'} img={'/images/SLogo/call.png'} />
        <SpecialityContainer title={'Authentic Products'} img={'/images/SLogo/product.png'} />
        <SpecialityContainer title={'Secure Payment'} img={'/images/SLogo/payment.png'} />
        <SpecialityContainer title={'Best Prices & Offers'} img={'/images/SLogo/offers.png'} />

    </div>;
};
export default Speciality;