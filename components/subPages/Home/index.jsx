
import Speciality from "../../reuseable/Speciality";
import ItemCollections from "./components/ItemCollections";
import Offers from "./components/Offers/components";
import Banner from './components/Banner/index'
import Slider from "../../reuseable/Slider";
import { useSelector } from "react-redux";
import brands from "./utils/Brands";
import CommonLayout from "../../../layouts/CommonLayout/CommonLayout";

const Home = () => {

    const categoryArray = useSelector(state => state.category.category)
    const weekDeals = useSelector(state => state.weekDeals.weekDeals)

    return <div>
        <CommonLayout>
            <Banner />
            <Slider dataArray={categoryArray} type={'category'} />
            <Offers />
            <Slider dataArray={weekDeals} type={'weekDeals'} />
            <Slider dataArray={brands} type={'brands'} />
            <ItemCollections />
            <Speciality />
        </CommonLayout>
    </div>;
};
export default Home;