import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import required modules
import { Pagination, Autoplay, Navigation } from "swiper";
import SliderContainer from "./components/SliderContainer";

export default function Banner() {

    const banners = [
        {
            id: 1,
            offer: 'Save up 30% off',
            title: 'Bengal Vegetable farm  Organic 100%',
            details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vehicula faucibus massa est elit maecenas.',
            img: '/images/banner.png',
        },
        {
            id: 2,
            offer: 'Save up 30% off',
            title: 'Fruits farm  Organic 100%',
            details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vehicula faucibus massa est elit maecenas.',
            img: '/images/banner.png',
        },
        {
            id: 3,
            offer: 'Save up 30% off',
            title: 'Fish, Meat farm  Organic 100%',
            details: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vehicula faucibus massa est elit maecenas.',
            img: '/images/banner.png',
        }

    ]

    return (
        <div className='relative h-[120vh] md:h-[80vh]'>

            <Swiper
                direction={"vertical"}
                pagination={{
                    clickable: true,
                }}
                autoplay={{
                    delay: 3000,
                    disableOnInteraction: false,
                }}
                modules={[Pagination, Autoplay, Navigation]}
                className="h-full w-full"
            >

                {
                    banners.map(banner => <SwiperSlide className="w-full h-full"
                        key={banner.id}>
                        <SliderContainer banner={banner} />
                    </SwiperSlide>)
                }

            </Swiper>

        </div>
    );
}
