/* eslint-disable @next/next/no-img-element */
import Image from 'next/image'

const SliderContainer = ({ banner }) => {

    const { offer, title, details, img } = banner
    return <div className="bg-[#EBFAEB] h-full w-full grid grid-cols-1 md:grid-cols-2 p-2 gap-2">
        {/* <div className="flex items-center h-[50vh] md:h-[80vh] px-5 md:px-10 lg:px-20 order-2"> */}
        <div className="px-5 md:px-10 lg:px-20 xl:pl-28 order-2 h-[50vh] md:h-[80vh] flex items-center md:order-1">
            <div>
                <h3 className="text-myGreen text-lg lg:text-2xl font-medium">{offer}</h3>
                <h1 className="my-2 xl:mt-4 xl:mb-6 font-bold text-heading_color xl:leading-[60px] text-2xl xl:text-5xl">{title}</h1>
                <p className="text-text_color  xl:leading-7 mb-5 xl:mb-10">{details}</p>
                <button className="bg-green text-white rounded-full px-5 py-2 xl:px-10 xl:py-3">Order Now</button>
            </div>
        </div>
        <div className="md:p-10 lg:p-20 order-1 md:order-2 flex justify-center md:justify-start items-center">
            <div>
                <img layout="fill" className=" max-h-[50vh] lg:max-h-[67vh] bg-cover" src={img} alt="Banner" />
            </div>
        </div>
    </div>
        ;
};
export default SliderContainer;