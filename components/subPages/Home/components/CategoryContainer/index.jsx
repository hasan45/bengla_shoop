import Image from "next/image";

const CategoryContainer = ({ data }) => {
    const src = data.img;
    return <div className="bg-[#F7F7F7] hover:bg-[#EBFAEB] rounded-md w-56 xl:w-64 h-56 flex justify-center items-center">
        <div>
            {/* <img className="h-28 w-28" src={data.img} alt="items" /> */}
            <Image unoptimized={true} height={112} width={112} loader={() => src} src={src} alt="items" />
            <h6 className="text-lg text-center mt-3">{data.name}</h6>
        </div>
    </div>
        ;
};
export default CategoryContainer;