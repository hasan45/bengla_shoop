const OfferContainer = ({ img }) => {
    return <div style={{ 'backgroundImage': `url(${img})` }} className='px-3 sm:px-5 md:px-16  lg:px-8 xl:px-16 py-8 md:py-16 xl:py-28 2xl:py-32 bg-center bg-cover rounded-2xl'>
        <h3 className="text-xl md:text-2xl text-orange1 font-medium">
            Buy 1 Get 1
        </h3>
        <h1 className="text-2xl md:text-4xl font-semibold mt-4 mb-6 md:mb-11">
            Fresh Fruits <br />
            Collection
        </h1>

        <button className="text-sm md:text-lg text-orange1 px-4 md:px-10 py-3 bg-white rounded-full font-semibold hover:bg-orange1 hover:text-white">Order Now</button>
    </div >;
};
export default OfferContainer;