import OfferContainer from "./OfferContainer";

const Offers = () => {
    return <div className="mt-24 px-0 md:px-5 grid grid-cols-1 lg:grid-cols-2 gap-5">
        <OfferContainer img={'/images/offers/offer1.png'} />
        <OfferContainer img={'/images/offers/offer2.png'} />
    </div>;
};
export default Offers;