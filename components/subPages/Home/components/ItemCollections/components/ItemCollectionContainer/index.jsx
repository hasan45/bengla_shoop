/* eslint-disable @next/next/no-img-element */
import { AiOutlineArrowRight } from 'react-icons/ai';

const ItemCollectionContainer = ({ title, img, color, mdSpan = false }) => {

    return <div style={{ 'backgroundColor': `${color}` }} className={`rounded-2xl grid grid-cols-1 sm:grid-cols-2 p-5 gap-3 ${mdSpan && 'md:col-span-2 xl:col-span-1'}`}>
        <div className={`px-3 flex items-center`}>
            <div>
                <h1 className="text-3xl md:text-2xl lg:text-3xl font-semibold">{title}</h1>
                <div className='flex justify-center items-center w-14 h-14 bg-white rounded-full text-orange1 hover:bg-orange1 hover:text-white mt-5'>
                    <AiOutlineArrowRight />
                </div>
            </div>
        </div>
        <img src={img} alt="Collection Item" />

    </div >;
};
export default ItemCollectionContainer;