import ItemCollectionContainer from "./components/ItemCollectionContainer";

const ItemCollections = () => {
    return <div className="mt-24 grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-5 px-0 md:px-5">

        <ItemCollectionContainer mdSpan={true} color={'#FFF0DC'} title={"Fresh Fruits Collection"} img={'/images/ItemsCollections/fruits.png'} />
        <ItemCollectionContainer color={'#DDF1D6'} title={"Vegetable Collection"} img={'/images/ItemsCollections/vegetable.png'} />
        <ItemCollectionContainer color={'#FFEBB7'} title={"Grocery Item"} img={'/images/ItemsCollections/grocery.png'} />

    </div>;
};
export default ItemCollections;