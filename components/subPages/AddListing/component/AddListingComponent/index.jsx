/* eslint-disable @next/next/no-img-element */
import { useState } from 'react';
import sdkProvider from '../../../../../utils/sdk';

const AddListingComponent = () => {

    const sdk = sdkProvider()

    const [listingDetails, setListingDetails] = useState(null);
    const handleSubmit = (e) => {
        e.preventDefault()
        const title = e.target.title.value;
        const description = e.target.description.value;
        const country = e.target.country.value;
        const state = e.target.state.value;
        const city = e.target.city.value;
        const street = e.target.street.value;
        const price = Number(e.target.price.value);

        const img = e.target.img.files[0];
        sdk.images.upload({
            image: img
        }, {
            expand: true
        }).then(res => {
            console.log(res);
            if (res?.status === 200) {
                sdk.ownListings.create({
                    title: title,
                    description: description,
                    price: { amount: price, currency: "USD" },
                    availabilityPlan: {
                        type: "availability-plan/day",
                        entries: [
                            {
                                dayOfWeek: "mon",
                                seats: 3
                            },
                            {
                                dayOfWeek: "fri",
                                seats: 1
                            }
                        ]
                    },
                    privateData: {
                        externalServiceId: "abcd-service-id-1234"
                    },
                    publicData: {
                        address: {
                            city: city,
                            country: country,
                            state: state,
                            street: street,
                        },
                        category: "road",
                        gears: 22,
                        rules: "This is a nice, bike! Please, be careful with it."
                    },
                    images: [
                        res.data.data.id.uuid,
                    ]
                }, {
                    expand: true,
                    include: ["images"]
                }).then(res => {
                    console.log('Finally created listing with image', res);
                    setListingDetails(res.data);
                    e.target.reset();
                });
            }
        });
        console.log('Our STATE:', listingDetails);
    }

    return <div>
        <h2 className='text-myGreen text-4xl font-semibold text-center mb-4'>
            Add New Listing
        </h2>
        <form onSubmit={handleSubmit}>

            <div className='w-2/4 mx-auto my-10'>
                <div className='inline-block'>
                    <div className='my-3 grid grid-cols-1 gap-3'>
                        <label className='text-blue-700 font-semibold' htmlFor="img">Select Listing Image</label>
                        <input type="file" name="img" id="img" accept="image/png, image/jpeg" />
                    </div>
                </div>
                <input type="text" name='title' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Title' required />

                <textarea type="text" name='description' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Description' required />

                <div className='flex'>
                    <input type="text" name='country' className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Country' required />
                    <input type="text" name='state' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='State' required />
                </div>
                <div className='flex'>
                    <input type="text" name='city' className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='City' required />
                    <input type="text" name='street' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Street' required />
                </div>

                <input type="number" name='price' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Price Per Hour' required />

                <button className='w-full py-3 rounded-lg bg-green text-white text-lg' type="submit">Add Listing</button>
            </div>

        </form>
        <hr />
        <hr />
        {listingDetails && <div>
            <h1 className='text-myGreen-500 text-4xl font-semibold text-center my-10'>Added Listing Details</h1>
            <div className='grid grid-cols-2 gap-5 mb-10 mx-12'>
                <div className=' flex justify-end items-center'>
                    <div className='text-center shadow-lg pb-5'>
                        <img className='max-w-md rounded-lg' src={listingDetails?.included[0]?.attributes?.variants?.default?.url} alt="" />
                        <button className='mt-5 px-3 py-1 bg-purple-700 text-white rounded-full'>Upload New Image</button>
                    </div>
                </div>
                <div className='space-y-8 shadow-lg p-5 rounded-lg flex items-center max-w-md'>
                    <div className=''>
                        <h1>Title: {listingDetails.data.attributes.title}</h1>
                        <h1>Description: {listingDetails.data.attributes.description}</h1>
                        <h1>Country: {listingDetails.data.attributes.publicData.address.country}</h1>
                        <h1>State: {listingDetails.data.attributes.publicData.address.state}</h1>
                        <h1>City: {listingDetails.data.attributes.publicData.address.city}</h1>
                        <h1>Street: {listingDetails.data.attributes.publicData.address.street}</h1>
                        <h1 className='text-orange-700 text-lg'>Price Per Hour: $<span className='text-3xl text-red-500'>{listingDetails.data.attributes.price.amount}</span></h1>
                    </div>
                </div>
            </div>
        </div>}
    </div>;
};
export default AddListingComponent;