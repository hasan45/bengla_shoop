import CommonLayout from "../../../layouts/CommonLayout/CommonLayout";
import AddListingComponent from "./component/AddListingComponent"
const AddListing = () => {
    return <div>
        <CommonLayout>
            <AddListingComponent />
        </CommonLayout>
    </div>;
};
export default AddListing;