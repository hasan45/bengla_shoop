/* eslint-disable @next/next/no-img-element */
import { useState } from 'react';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { loadUserData } from '../../../store/currentUserSlice';
import sdkProvider from '../../../utils/sdk';

const Login = () => {

    // ShareTribe info
    const sdk = sdkProvider()

    // const dispatch = useDispatch()

    const router = useRouter()

    const [login, setLogin] = useState(true);

    const LoginAction = (email, password) => {
        sdk.login({
            username: email,
            password: password
        }).then(loginRes => {
            console.log("Login successful.")
            console.log(loginRes)
            if (loginRes?.status === 200) {
                toast.success('Login Successfull')
                router.push('/')
            }
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const email = e.target.email.value
        const password = e.target.password.value

        if (login) {
            LoginAction(email, password)
        }
        else {
            const firstName = e.target.firstName.value
            const lastName = e.target.lastName.value
            const displayName = e.target.displayName.value
            const age = Number(e.target.age.value)
            const phone = e.target.phone.value

            sdk.currentUser
                .create(
                    {
                        email: email,
                        password: password,
                        firstName: firstName,
                        lastName: lastName,
                        displayName: displayName,
                        bio: `Hello, I am ${displayName}`,
                        publicData: {
                            age: age,
                        },
                        protectedData: {
                            phoneNumber: phone,
                        },
                        privateData: {
                            discoveredServiceVia: 'Twitter',
                        },
                    },
                    {
                        expand: true,
                    }
                )
                .then((res) => {
                    console.log(res);
                    if (res.status === 200) {
                        setLogin(true)
                        LoginAction(email, password)
                    }
                });
        }

    };

    return <div className='container mx-auto mb-10 px-2 sm:px-5 py-5'>

        <div className='mx-auto mt-3 max-w-md text-center border-2 p-5 rounded-lg bg-slate-50'>
            <h2 className='text-myGreen text-4xl font-semibold  text-center mb-4'>
                {login ? 'Login' : 'Sign Up'}
            </h2>

            <form onSubmit={handleSubmit}>

                {
                    login ? '' :
                        <>
                            <div className='flex'>
                                <input type="text" name='firstName' className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='First Name' required />
                                <input type="text" name='lastName' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Last Name' required />
                            </div>
                            <input type="text" name='displayName' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Display Name' required />

                            <input type="text" name='age' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Your Age' required />

                            <input type="text" name='phone' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Phone Number' required />
                        </>
                }

                <input type="email" name="email" id="email" className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-900 my-2 rounded-md' placeholder='Email Address' required />

                <input type="password" name="password" id="password" className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Enter Password' required />

                {
                    login ? '' : <input type="password" name="confirmPassword" id="confirmPassword" className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Confirm Password' required />
                }

                <button type="submit" className='w-full p-3 rounded-full bg-red-500 hover:bg-red-600 text-white my-3'>
                    {
                        login ? 'Login' : 'Sign Up'
                    }
                </button>

            </form>

            <p className='mt-3 text-lg'>
                {login ? "Don't have an account.?" : "Already Have an account.?"}

                <button onClick={() => setLogin(!login)} className='text-red-600 ml-1.5'>
                    {login ? "Sign Up" : "Login"}
                </button>
            </p>
            {
                login ?
                    <p className='mt-3'>
                        <button className='text-red-600'>
                            Forgot Password.?
                        </button>
                    </p>
                    : ''
            }

            <div className='grid grid-cols-7 mb-3 mt-5 items-center'>
                <div className='bg-gray-900 h-0.5 col-span-3'></div>
                <span>OR</span>
                <div className='bg-gray-900 h-0.5 col-span-3'></div>
            </div>

            <div>
                <button className='w-full bg-white flex items-center justify-center mb-3 border-2 my-2 rounded-md h-12'>
                    <span className='mr-1'><img src="https://img.icons8.com/color/48/000000/google-logo.png" alt='google login' /></span>
                    Continue With Google</button>

                <button className='flex items-center justify-center w-full bg-zinc-800 my-2 text-white rounded-md h-12 '>
                    <span className='mr-1'>
                        <img src="https://img.icons8.com/color/48/000000/github--v1.png" alt='Github login' />
                    </span>
                    Continue With Github</button>
            </div>

        </div>
    </div>
};
export default Login;