import CommonLayout from "../../../layouts/CommonLayout/CommonLayout";
import CartContainer from "./components/cartContainer";

const Cart = () => {
    return <div>
        <CommonLayout>
            <CartContainer />
        </CommonLayout>
    </div>;
};
export default Cart;