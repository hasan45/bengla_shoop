/* eslint-disable @next/next/no-img-element */
const CartDiv = ({ cart }) => {
    const { img, name, price } = cart.product

    return <tr className="p-2 border-2 my-1">
        <td className="border border-slate-300 text-center sm:px-2 px-0.5 "><img className="w-20 mx-auto" src={img} alt="product" /></td>
        <td className="border border-slate-300 text-center sm:px-2 px-0.5">{name}</td>
        <td className="border border-slate-300 text-center sm:px-2 px-0.5">{cart.quantity}</td>
        <td className="border border-slate-300 text-center sm:px-2 px-0.5">$
            {
                cart.quantity * price
            }
        </td>
    </tr>
};
export default CartDiv;