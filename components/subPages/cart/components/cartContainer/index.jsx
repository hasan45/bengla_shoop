import { useState } from "react";
import { useSelector } from "react-redux";
import CartDiv from "./components/CartDiv";

const CartContainer = () => {
    const cartData = useSelector(state => state.cart.cart)

    let totalPrice = 0

    cartData.forEach(element => {
        totalPrice += (Number(element.quantity) * Number(element.product.price))
    });

    return <div>
        <table className="border-collapse border border-slate-400 mx-auto">
            <thead>
                <tr>
                    <th className="border border-slate-300 px-0.5 sm:px-2">Photo</th>
                    <th className="border border-slate-300 px-0.5 sm:px-2">Name</th>
                    <th className="border border-slate-300 px-0.5 sm:px-2">Price</th>
                    <th className="border border-slate-300 px-0.5 sm:px-2">Total</th>
                </tr>
            </thead>
            <tbody>
                {
                    cartData.map(cart => <CartDiv key={cart.product.sku} cart={cart}></CartDiv>)
                }
            </tbody>
        </table>
        <h1 className="mx-2 text-2xl my-5 text-center">Total Price: $ {totalPrice}</h1>

    </div>;
};
export default CartContainer;