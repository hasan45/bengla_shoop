import { loadDisplayProduct } from '../../../../../store/displayProductSlice';
import { store } from '../../../../../store/store'

const CategoryFilter = (allProducts, route, minPrice, maxprice) => {

    if (route.length === 0) {
        const filteredProduct = allProducts.filter(element =>
            element.price >= minPrice && element.price <= maxprice
        )
        store.dispatch(loadDisplayProduct(filteredProduct))
    }
    else if (route.length === 1) {
        const filteredProduct = allProducts.filter(element =>
            element.category.toLowerCase() === route[0].toLowerCase()
            && element.price >= minPrice && element.price <= maxprice
        )
        store.dispatch(loadDisplayProduct(filteredProduct))
    }
    else if (route.length === 2) {
        const filteredProduct = allProducts.filter(element =>
            element.subCategory.toLowerCase() === route[1].toLowerCase()
            && element.price >= minPrice && element.price <= maxprice
        )
        store.dispatch(loadDisplayProduct(filteredProduct))
    }
};
export default CategoryFilter;