import CommonLayout from "../../../layouts/CommonLayout/CommonLayout";
import Speciality from "../../reuseable/Speciality";
import CategoryMenu from "./components/CategoryMenu";
import ProductsTray from "./components/ProductsTray";

const Shop = ({ index }) => {
    return <CommonLayout>
        <div className="grid grid-cols-6 sm:grid-cols-10 lg:grid-cols-4 xl:grid-cols-5">
            <CategoryMenu index={index} />
            <ProductsTray index={index} />
        </div>
        <Speciality />
    </CommonLayout>;
};
export default Shop;