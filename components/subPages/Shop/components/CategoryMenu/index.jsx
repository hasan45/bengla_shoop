/* eslint-disable @next/next/no-img-element */
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { AiOutlineRight } from 'react-icons/ai'
import { useDispatch, useSelector } from 'react-redux';
import { change } from '../../../../../store/OpenCategorySlice';
import CategoryFilter from '../../utils/Filter/index.js';
import Categories from './components/Categories';
import PriceRange from './components/PriceRange';

const CategoryMenu = ({ index }) => {

    const dispatch = useDispatch()
    const categoryMenu = useSelector(state => state.OpenCategory.openCategory)
    const allProducts = useSelector(state => state.products.products)
    const router = useRouter()

    // State for keeping the price range
    const [minPrice, setMinPrice] = useState(0)
    const [maxPrice, setMaxPrice] = useState(900)

    // States for controling category menu
    const [showCategory, setShowCategory] = useState(false)
    const [showPriceRange, setShowPriceRange] = useState(false)


    // Filter Function call
    useEffect(() => {
        CategoryFilter(allProducts, index, minPrice, maxPrice)
    }, [minPrice, maxPrice, allProducts, index])

    return <div className="pr-2 lg:px-5">

        <div className='sticky top-10 lg:w-52 xl:w-[212px] 2xl:w-60 3xl:w-72 z-50 bg-white '>

            {/* Open the menu button container */}
            <div onClick={() => dispatch(change())} className='lg:hidden relative'>
                <img src="https://img.icons8.com/fluency/48/undefined/menu--v1.png" alt='nav menu' className='w-6 h-6' />
            </div>

            {/* Category Menu */}
            <div className={`${categoryMenu ? 'left-0' : 'left-[-600px]'} absolute lg:static w-56 lg:w-full  duration-300 ease-out top-10 bg-white rounded-lg py-5 pr-2 `}>

                <div className='border-[0.5px] rounded-md pb-5'>

                    <div className="flex items-center justify-between w-full px-4 py-3 text-heading_color bg-[#EBFAEB] text-lg 3xl:text-xl font-semibold cursor-pointer relative"
                        onClick={() => setShowCategory(!showCategory)}
                    >
                        Categories
                        <AiOutlineRight className={`${showCategory && '-rotate-90'}`} />
                    </div>

                    {/* For Menues */}
                    <div className={`${showCategory ? 'block' : 'hidden'}`}>
                        <Categories />
                    </div>

                </div>

                {/* Price Filterations */}
                <div className='border-[0.5px] rounded-md mt-5 pb-5 cursor-pointer'>
                    <div className="flex items-center justify-between w-full px-4 py-3 text-heading_color bg-[#EBFAEB] text-lg 3xl:text-xl font-semibold"
                        onClick={() => setShowPriceRange(!showPriceRange)}
                    >
                        <p>Price</p>
                        <AiOutlineRight className={`${showPriceRange && '-rotate-90'}`} />
                    </div>

                    <div className={`px-4 mt-4 ${showPriceRange ? 'block' : 'hidden'}`}>
                        <PriceRange
                            min={0}
                            max={900}
                            onChange={({ min, max }) => {
                                setMaxPrice(max)
                                setMinPrice(min)
                            }}
                        />
                    </div>
                </div>

                <div className='text-center'>
                    <button
                        onClick={() => {
                            router.push('/shop')
                            setMaxPrice(900)
                            setMinPrice(0)
                        }}
                        className='mt-5 w-full text-myGreen font-semibold border-2 hover:bg-green hover:text-white rounded-full px-5 py-2'>Clear Filters</button>
                </div>

            </div>
        </div >
    </div >;
};
export default CategoryMenu;