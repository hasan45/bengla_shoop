const categories = [
    {
        category: 'Vegetables',
        img: '/images/categories/vegetable.png',
    },
    {
        category: 'Fruits',
        img: '/images/categories/fruits.png',
    },
    {
        category: 'Groceries',
        img: '/images/categories/Groceries.png',
    },
    {
        category: 'Meat',
        img: '/images/categories/meat.png',
    },
    {
        category: 'Fish',
        img: '/images/categories/fish.png',
    },
    {
        category: 'Bevarage',
        img: '/images/categories/Bevarage.png',
    },
    {
        category: 'Dry Food',
        img: '/images/categories/dryFood.png',
    }
]

export default categories