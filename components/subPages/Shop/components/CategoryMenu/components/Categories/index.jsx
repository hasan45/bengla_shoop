/* eslint-disable @next/next/no-img-element */
import { useRouter } from "next/router";
import { useState } from "react";
import { AiOutlineRight } from "react-icons/ai";

const Categories = () => {

    const router = useRouter()

    const [openFruits, setOpenFruits] = useState(false)

    return <div>
        <div onClick={() => { router.push(`/shop/vegetables`) }} className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/vegetable.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Vegetables</h6>
            </div>
            <AiOutlineRight />
        </div>

        <div onClick={() => {
            router.push(`/shop/fruits`)
            setOpenFruits(!openFruits)
        }}
            className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/fruits.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Fruits</h6>
            </div>
            <AiOutlineRight />
        </div>

        <div className={`border-l-2 ml-8 ${openFruits ? 'block' : 'hidden'}`}>
            <div className="pl-6 mt-3 cursor-pointer bg-white" onClick={() => { router.push(`/shop/fruits/green_fruits`) }}>Green Fruits</div>
            <div className="pl-6 mt-3 cursor-pointer bg-white" onClick={() => { router.push(`/shop/fruits/yellow_fruits`) }}>Yellow Fruits</div>
            <div className="pl-6 mt-3 cursor-pointer bg-white" onClick={() => { router.push(`/shop/fruits/red_fruits`) }}>Red Fruits</div>
            <div className="pl-6 mt-3 cursor-pointer bg-white" onClick={() => { router.push(`/shop/fruits/white_fruits`) }}>White Fruits</div>
        </div>

        <div onClick={() => { router.push(`/shop/groceries`) }} className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/Groceries.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Groceries</h6>
            </div>
            <AiOutlineRight />
        </div>

        <div onClick={() => { router.push(`/shop/meat`) }} className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/meat.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Meat</h6>
            </div>
            <AiOutlineRight />
        </div>

        <div onClick={() => { router.push(`/shop/fish`) }} className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/fish.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Fish</h6>
            </div>
            <AiOutlineRight />
        </div>

        <div onClick={() => { router.push(`/shop/bevarage`) }} className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/bevarage.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Bevarage</h6>
            </div>
            <AiOutlineRight />
        </div>

        <div onClick={() => { router.push(`/shop/dry_Food`) }} className='flex justify-between px-4 items-center mt-2 cursor-pointer bg-white'>
            <div className='flex items-center mt-2'>
                <img src={'/images/categories/dryFood.png'} alt="category logo" />
                <h6 className='text-heading_color 3xl:text-lg ml-2'>Dry Food</h6>
            </div>
            <AiOutlineRight />
        </div>


    </div>;
};
export default Categories;