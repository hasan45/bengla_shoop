const Banner = () => {
    return <div style={{ 'backgroundImage': `url(/images/herobg.png)` }}
        className=' bg-center bg-cover rounded-lg h-72 w-full flex items-center'>
        <div className="ml-5 md:ml-10 lg:ml-20">
            <h6 className="font-medium text-xl text-orange1 mb-3">Buy 1 Get 1</h6>
            <h1 className="text-myGreen text-2xl md:text-4xl font-bold leading-8 md:leading-[50px]">
                Up to 30% Discount <br />
                on Selected Items
            </h1>
        </div>
    </div>;
};
export default Banner;