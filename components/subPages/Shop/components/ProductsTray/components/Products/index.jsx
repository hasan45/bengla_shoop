import ProductContainer from "../../../../../../reuseable/ProductContainer";

const Products = ({ productCollections }) => {

    if (productCollections.length === 0) {
        return <div className="text-3xl text-center py-10 font-semibold">No Data Found</div>
    }

    return <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 3xl:grid-cols-4 gap-10">
        {
            productCollections.map(product => <ProductContainer key={product.sku} product={product} />)
        }

    </div>;
};
export default Products;