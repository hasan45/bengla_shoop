import { BsChevronDown, BsFillGrid3X3GapFill } from 'react-icons/bs'
import { Icon } from '@iconify/react';

const Status = ({ productCollections, index }) => {

    return <div className="mb-10">
        <h1 className="text-4xl font-bold mt-12 mb-4">Fruits Collection</h1>
        <div className="grid grid-cols-1 lg:grid-cols-2">
            <div className='flex mb-5 lg:mb-0'>
                <p className='text-[#828282] mr-2'>Shop {index.length > 0 && '|'}</p>
                {
                    index.map((param, i) => <div className='mr-2' key={i}>
                        <p className={`${index.length === (i + 1) ? 'text-heading_color font-semibold' : 'text-[#828282] font-normal'} capitalize`}>{param}
                            {
                                index.length === (i + 1) ? '' : <span className={`ml-2`}>|</span>
                            }
                        </p>
                    </div>)
                }
            </div>
            <div className="md:ml-auto flex items-center flex-wrap justify-end">
                <p className='mr-6'>{productCollections.length} Products Found</p>
                <Icon icon="uis:list-ui-alt" className='text-xl mr-6 cursor-pointer' />
                <BsFillGrid3X3GapFill className='cursor-pointer' />
                <p className="ml-6 flex items-center cursor-pointer">
                    Default Sorting
                    <span className='ml-1'><BsChevronDown className='text-sm' /></span>
                </p>
            </div>
        </div>
    </div >;
};
export default Status;