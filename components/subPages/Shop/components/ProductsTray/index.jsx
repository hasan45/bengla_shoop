import { useSelector } from "react-redux";
import Banner from "./components/Banner";
import Products from "./components/Products";
import Status from "./components/Status/index";

const ProductsTray = ({ index }) => {
    const productCollections = useSelector(state => state.displayProduct.displayProduct)
    return <div className="col-span-5 sm:col-span-9 lg:col-span-3 xl:col-span-4">
        <Banner />
        <Status index={index} productCollections={productCollections} />
        <Products productCollections={productCollections} />
    </div>;
};
export default ProductsTray;