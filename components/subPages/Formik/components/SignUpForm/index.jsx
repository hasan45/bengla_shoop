import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const SignUpForm = () => {
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            email: '',
        },
        validationSchema: Yup.object({
            firstName: Yup.string()
                .max(15, 'Must be 15 characters or less')
                .required('Required'),
            lastName: Yup.string()
                .max(20, 'Must be 20 characters or less')
                .required('Required'),
            email: Yup.string().email('Invalid email address').required('Required'),
        }),
        onSubmit: values => {
            alert(JSON.stringify(values, null, 2));
        },
    });
    return (
        <form className='w-2/4 mx-auto my-5' onSubmit={formik.handleSubmit}>
            <h1 className='text-3xl text-center my-5 text-myGreen'>Lets Use FORMIK with YUP</h1>
            <label htmlFor="firstName">First Name</label>
            <input
                className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md'
                id="firstName"
                name="firstName"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.firstName}
            />
            {formik.touched.firstName && formik.errors.firstName ? (
                <div className='text-red-500'>{formik.errors.firstName}</div>
            ) : null}

            <label htmlFor="lastName">Last Name</label>
            <input
                className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md'
                id="lastName"
                name="lastName"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.lastName}
            />
            {formik.touched.lastName && formik.errors.lastName ? (
                <div className='text-red-500'>{formik.errors.lastName}</div>
            ) : null}

            <label htmlFor="email">Email Address</label>
            <input
                className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md'
                id="email"
                name="email"
                type="email"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.email}
            />
            {formik.touched.email && formik.errors.email ? (
                <div className='text-red-500'>{formik.errors.email}</div>
            ) : null}

            <button className='bg-orange-700 text-white my-3 w-full py-3 rounded-lg' type="submit">Submit</button>
        </form>
    );
};

export default SignUpForm;

// className='bg-orange-700 text-white my-3 w-full py-3 rounded-lg'
// className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md'