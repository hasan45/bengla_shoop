import Sidebar from "../Sidebar";

const ProfileSidebar = ({ children }) => {
    return <div>
        <main className="grid grid-cols-6">
            <Sidebar />
            <div className="col-span-5">
                {children}
            </div>
        </main>
    </div>;
};
export default ProfileSidebar;