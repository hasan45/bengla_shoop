import Link from "next/link";

const Sidebar = () => {
    return <div className="space-y-5">
        <p>
            <Link href={'/profile'}>
                <a>
                    Profile
                </a>
            </Link>
        </p>
        <p>
            <Link href={'/profile/updateProfile'}>
                <a>
                    Profile Update
                </a>
            </Link>
        </p>
        {/* <Link href={'/'}>Profile Delete</Link> */}
        <p>
            <Link href={'/profile/changePassword'}>
                <a>
                    Change Password
                </a>
            </Link>
        </p>
        <p>
            <Link href={'/profile/changeEmail'}>
                <a>
                    Change Email
                </a>
            </Link>
        </p>
    </div>;
};
export default Sidebar;