import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import CommonLayout from "../../../../layouts/CommonLayout/CommonLayout";
import { loadUserData } from "../../../../store/currentUserSlice";
import sdkProvider from "../../../../utils/sdk";
import ProfileSidebar from "../layout/ProfileSidebar";

const ChangeEmail = () => {

    const sdk = sdkProvider()
    const router = useRouter()
    const dispatch = useDispatch()

    const handleSubmit = (e) => {
        e.preventDefault()
        const newEmail = e.target.email.value
        const password = e.target.password.value

        sdk.currentUser.changeEmail({
            currentPassword: password,
            email: newEmail
        }, {
            expand: true
        }).then(res => {
            console.log(res)
            if (res.status === 200) {
                sdk.currentUser.show().then(res => {
                    dispatch(loadUserData(res?.data?.data?.attributes));
                    toast('Email Update Successfull. Verify Your email', { id: 'Update Email' })
                    router.push('/profile')
                });
            }
        });
    }

    return <CommonLayout>
        <ProfileSidebar>
            <div className='mx-auto mt-3 max-w-md text-center border-2 p-5 rounded-lg bg-slate-50'>
                <h2 className='text-myGreen text-4xl font-semibold  text-center mb-4'>
                    Change Email
                </h2>

                <form onSubmit={handleSubmit}>

                    <input type="email" name='email' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Enter New Email' required />

                    <input type="password" name='password' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Current Password' required />

                    <button type="submit" className='w-full p-3 rounded-full bg-red-500 hover:bg-red-600 text-white my-3'>
                        Update
                    </button>
                </form>
            </div>
        </ProfileSidebar>
    </CommonLayout>;
};
export default ChangeEmail;