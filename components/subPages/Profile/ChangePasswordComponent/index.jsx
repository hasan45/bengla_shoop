import { useRouter } from "next/router";
import { toast } from "react-toastify";
import CommonLayout from "../../../../layouts/CommonLayout/CommonLayout";
import sdkProvider from "../../../../utils/sdk";
import ProfileSidebar from "../layout/ProfileSidebar";

const ChangePasswordComponent = () => {

    const sdk = sdkProvider()
    const router = useRouter()

    const handleSubmit = (e) => {
        e.preventDefault()
        const oldPassword = e.target.old.value
        const newPassword = e.target.new.value

        sdk.currentUser.changePassword({
            currentPassword: oldPassword,
            newPassword: newPassword
        }, {
            expand: true
        }).then(res => {
            console.log(res)
            if (res.status === 200) {
                router.push('/profile')
                toast.success('Password Changed Successfully')
            }
        });
    }

    return <CommonLayout>
        <ProfileSidebar>
            <div className='mx-auto mt-3 max-w-md text-center border-2 p-5 rounded-lg bg-slate-50'>
                <h2 className='text-myGreen text-4xl font-semibold  text-center mb-4'>
                    Change Password
                </h2>

                <form onSubmit={handleSubmit}>

                    <input type="password" name='old' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Current Password' required />

                    <input type="password" name='new' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='New Password' required />

                    <button type="submit" className='w-full p-3 rounded-full bg-red-500 hover:bg-red-600 text-white my-3'>
                        Update
                    </button>
                </form>
            </div>
        </ProfileSidebar>
    </CommonLayout>;
};
export default ChangePasswordComponent;