import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import CommonLayout from "../../../../layouts/CommonLayout/CommonLayout";
import { loadUserData } from "../../../../store/currentUserSlice";
import sdkProvider from "../../../../utils/sdk";
import ProfileSidebar from "../layout/ProfileSidebar";

const UpdateProfileComponent = () => {

    // ShareTribe info
    const sdk = sdkProvider()
    const router = useRouter()
    const dispatch = useDispatch()

    const handleSubmit = (e) => {
        e.preventDefault()
        const firstName = e.target.firstName.value
        const lastName = e.target.lastName.value
        const displayName = e.target.displayName.value
        const age = Number(e.target.age.value)
        const phone = e.target.phone.value

        const img = e.target.img.files[0];

        sdk.images.upload({
            image: img
        }, {
            expand: true
        }).then(res => {
            console.log('image update', res);
            if (res?.status === 200) {

                sdk.currentUser.updateProfile({
                    firstName: firstName,
                    lastName: lastName,
                    displayName: displayName,
                    bio: `Hello, I am ${displayName}`,

                    profileImageId: res.data.data.id.uuid,

                    publicData: {
                        age: age,
                        imgURL: res.data.data.attributes.variants.default.url,
                    },
                    protectedData: {
                        phoneNumber: phone
                    },
                    privateData: {
                        discoveredServiceVia: "Twitter"
                    }
                }, {
                    expand: true,
                    include: ["profileImage"]
                }).then(res => {
                    console.log(res.data)
                    if (res.status === 200) {
                        console.log('updated user', res)
                        // sdk.currentUser.show().then(res => {
                        //     // dispatch(loadUserData(res?.data?.data?.attributes));
                        //     console.log('Current User', res)
                        //     router.push('/profile')
                        // });
                        toast.success('Update Successfull')
                        router.push('/profile')
                    }
                });
            }
        });
        e.target.reset()
    }

    return <CommonLayout>

        <ProfileSidebar>
            <div className='mx-auto mt-3 max-w-md text-center border-2 p-5 rounded-lg bg-slate-50'>
                <h2 className='text-myGreen text-4xl font-semibold text-center mb-4'>
                    Update Profile
                </h2>

                <form onSubmit={handleSubmit}>

                    <div className='text-left'>
                        <div className='my-3 grid grid-cols-1 gap-3'>
                            <label className='text-blue-700 font-semibold' htmlFor="img">Select Listing Image</label>
                            <input type="file" name="img" id="img" accept="image/png, image/jpeg" />
                        </div>
                    </div>

                    <div className='flex'>
                        <input type="text" name='firstName' className='bg-gray-200 w-full p-3 mr-1 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='First Name' required />
                        <input type="text" name='lastName' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Last Name' required />
                    </div>
                    <input type="text" name='displayName' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Display Name' required />

                    <input type="text" name='age' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Your Age' required />

                    <input type="text" name='phone' className='bg-gray-200 w-full p-3 focus:outline-red-300 text-gray-800 my-2 rounded-md' placeholder='Phone Number' required />

                    <button type="submit" className='w-full p-3 rounded-full bg-red-500 hover:bg-red-600 text-white my-3'>
                        Update
                    </button>
                </form>
            </div>
        </ProfileSidebar>
    </CommonLayout>;
};
export default UpdateProfileComponent;