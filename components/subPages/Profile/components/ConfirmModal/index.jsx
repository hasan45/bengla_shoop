const sharetribeSdk = require('sharetribe-flex-sdk');
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { loadUserData } from "../../../../../store/currentUserSlice";
import sdkProvider from "../../../../../utils/sdk";

const DeleteConfirmationModal = ({ setOpenModal }) => {
    // ShareTribe info

    const sdk = sdkProvider()

    // const dispatch = useDispatch()
    const router = useRouter()

    const handleSubmit = (e) => {
        e.preventDefault()
        const password = e.target.password.value;
        console.log(password)

        sdk.currentUser.delete({
            currentPassword: password,
        }, {
            expand: true
        }).then(res => {
            console.log(res.data)
            if (res.status === 200) {
                toast.success('Account Deleted Successfully')
                router.push('/')
            }
        });
    }

    return <div className="flex justify-center items-center h-screen w-screen z-[1000]">
        <div className="bg-white relative rounded-3xl p-5">

            <button onClick={() => setOpenModal(false)} className="bg-red-600 text-white px-3 py-1 
            rounded-full font-semibold absolute top-5 right-5">X</button>

            <form onSubmit={handleSubmit} className="grid grid-cols-1 gap-5 mt-10">
                <input type="password" name="password" id="password" className="px-3 py-2 bg-gray-200 rounded-md" placeholder="Password" />
                <button className="px-5 py-2 bg-red-500 text-white rounded-full" type="submit">Confirm Delete</button>
            </form>

        </div>
    </div>;
};
export default DeleteConfirmationModal;