/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import DeleteConfirmationModal from "../ConfirmModal";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import isUser from "../../../../../utils/isUser.js";
import sdkProvider from "../../../../../utils/sdk";

const ProfileDisplay = () => {

    const [userData, setUserData] = useState()

    useEffect(() => {
        const sdk = sdkProvider()

        sdk.currentUser.show().then(res => {
            console.log(res)
            setUserData(res.data.data.attributes)
        })

    }, [])

    const router = useRouter()
    const [openModal, setOpenModal] = useState(false)

    const { authUser, loading } = isUser();

    if (loading) {
        return <h1 className="text-7xl text-center">Loading...</h1>
    }

    if (!authUser) {
        router.push('/login')
    }

    console.log(userData)

    return <div>
        <div>
            <h1 className="text-4xl font-semibold text-center">Profile</h1>
            <div className="px-2 md:px-5 py-5 shadow-xl rounded-xl grid grid-cols-1 gap-5 font-semibold">

                <img src={userData?.profile?.publicData?.imgURL} alt="Profile User" className="max-w-xs rounded-full" />

                <h1 className="text-3xl font-semibold">Name: <span className="text-myGreen">{userData?.profile?.displayName}</span> </h1>

                <h3 className="text-lg">Email: {userData?.email}</h3>

                <h3 className="text-lg">Phone: {userData?.profile?.protectedData?.phoneNumber}</h3>

                <h3 className="text-lg">Age: {userData?.profile?.publicData?.age}</h3>

                <h3 className="text-lg">Bio: {userData?.profile?.bio}</h3>

            </div>
            <div className="mt-5">

                {/* <button onClick={() => setOpenModal(true)} className="px-5 py-2 bg-red-500 text-white rounded-full">
                    Delete Account
                </button> */}

                <Link href={'/profile/updateProfile'}>
                    <a className="px-5 py-2 bg-purple-700 mx-1 text-white rounded-full">
                        Update Profile
                    </a>
                </Link>
            </div>

            {/* <div className={`${openModal ? 'block' : 'hidden'} fixed top-0 bottom-0 left-0 right-0 z-[1000] bg-[#00000080]`}>
                <DeleteConfirmationModal setOpenModal={setOpenModal} />
            </div> */}

        </div>
    </div>;
};
export default ProfileDisplay;