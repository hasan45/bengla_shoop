import CommonLayout from "../../../layouts/CommonLayout/CommonLayout";
import ProfileDisplay from "./components/ProfileDisplay";
import ProfileSidebar from "./layout/ProfileSidebar";

const ProfileComponent = () => {

    return <CommonLayout>
        <ProfileSidebar>
            <ProfileDisplay />
        </ProfileSidebar>
    </CommonLayout >;
};
export default ProfileComponent;