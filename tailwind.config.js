/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      'xs': '411px',
      'sm': '540px',

      'smd': '640px',
      'md': '768px',

      'lg': '1024px',
      'xl': '1280px',

      '2xl': '1400px',
      '3xl': '1920px',
    },
    extend: {
      colors: {
        heading_color: '#333333',
        text_color: '#4F4F4F',
        orange1: '#FF5C00',
        myGreen: '#27AE60',
      },
    },
  },
  plugins: [],
}
