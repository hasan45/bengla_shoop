import Login from "../../components/subPages/Login";
import CommonLayout from "../../layouts/CommonLayout/CommonLayout";

const LoginPage = () => {
    return <div>
        <CommonLayout>
            <Login />
        </CommonLayout>
    </div>;
};
export default LoginPage;