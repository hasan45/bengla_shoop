import HomePage from "../components/subPages/Home"
import { loadCategory } from "../store/categorySlice"
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { loadWeekDeals } from "../store/weekDealsSlice";

export default function Home({ category, products }) {

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(loadCategory(category))
  }, [dispatch, category])

  useEffect(() => {
    dispatch(loadWeekDeals(products))
  }, [dispatch, products])

  return (
    <div>
      <HomePage />
    </div>
  )
}

export async function getStaticProps() {
  const response = await fetch(`https://bengal-food-server-bxh0xpie0-mohammadamithasan.vercel.app/api/category`)
  const data = await response.json()

  const productResponse = await fetch(`https://bengal-food-server-bxh0xpie0-mohammadamithasan.vercel.app/api/weekDeals`)
  const loadWeekDeals = await productResponse.json()

  return {
    props: {
      category: data,
      products: loadWeekDeals,
    },
    revalidate: 300
  }
}