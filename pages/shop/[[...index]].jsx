import { useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import ShopComponents from "../../components/subPages/Shop";
import { loadProducts } from "../../store/productsSlice";
import { loadDisplayProduct } from "../../store/displayProductSlice";

const Shop = ({ productCollections }) => {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(loadProducts(productCollections))
        dispatch(loadDisplayProduct(productCollections))
    }, [dispatch, productCollections])

    const router = useRouter()
    const { index = [] } = router.query

    return <div>
        <ShopComponents index={index} />
    </div>;
};
export default Shop;

export async function getServerSideProps() {
    const response = await fetch(`https://bengal-food-server.vercel.app/api/collections`)
    const data = await response.json()

    return {
        props: {
            productCollections: data
        }
    }
}