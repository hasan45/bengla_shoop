import '../styles/globals.css'

import "swiper/css/bundle";
import { Provider } from 'react-redux';
import { store } from '../store/store';
import { ToastContainer } from 'react-toastify';

function MyApp({ Component, pageProps }) {
  return <Provider store={store}>
    <Component {...pageProps} />
    <ToastContainer />
  </Provider>
}

export default MyApp
